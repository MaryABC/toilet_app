package com.example.user.toiletapp1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import java.net.UnknownHostException;

public class MainActivity extends AppCompatActivity {

    EditText ed;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ed = (EditText) findViewById(R.id.editText);
        setTitle("Choose Radius");
    }

    public void openMap(View v){
        Intent t= new Intent(this, MapsActivity.class);
        t.putExtra("kind","101");
        try{t.putExtra("radius",Double.parseDouble(ed.getText().toString()));}
        catch (NumberFormatException ex){t.putExtra("radius",0);}
        startActivity(t);
    }
}
