package com.example.user.toiletapp1;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.util.Log;
import android.widget.Toast;
import android.view.ViewGroup.LayoutParams;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.w3c.dom.Document;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.Socket;
import java.net.URL;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    public GoogleMap mMap; 
    static final int REQUEST_IMAGE_CAPTURE = 1;
    String[][] toiletRecords;
    String ans;
    boolean flag = false;
    private static final int SERVER_PORT = 8888;
    byte[] ipAddr = new byte[]{(byte) 35, (byte) 162, (byte) 187, (byte) 156};
    private InetAddress SERVER_IP = null;
    Marker chosenToilet = null;
    Button btn1, btn2, btn3, btn4, btn5;
    TextView tv_tn, tv_oh;
    RatingBar rb,rbc,rbr;
    ImageView iv;
    GridLayout gl1, gl2;
    DownloadTask downloadTask = null;
    int routesDoneInActivity=0;
    /*
    attributes of the toiletRecords array:
    [0] toilet id
    [1] toilet name
    [2] lon
    [3] lat
    [4] price
    [5] clean
    [6] hours
    [7] rank
    [8] delcounter
    [9] image size
    [10] image
    [11] ratecount
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        btn1 = (Button) findViewById(R.id.button10);
        btn2 = (Button) findViewById(R.id.button8);
        btn3 = (Button) findViewById(R.id.button9);
        btn4 = (Button) findViewById(R.id.route);
        tv_tn = (TextView) findViewById(R.id.toiletName);
        tv_oh = (TextView) findViewById(R.id.openHours);
        rb = (RatingBar) findViewById(R.id.ratingBar);
        iv = (ImageView) findViewById(R.id.imageView4);
        gl1= (GridLayout) findViewById(R.id.gl1);
        gl2= (GridLayout) findViewById(R.id.gl2);

        rbc = (RatingBar) findViewById(R.id.ratingBarClean);
        rbr = (RatingBar) findViewById(R.id.ratingBarRank);
        btn5 = (Button) findViewById(R.id.button11);

        rbc.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                if(rating<1.0f)
                    ratingBar.setRating(1.0f);
            }
        });
        rbr.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                if(rating<1.0f)
                    ratingBar.setRating(1.0f);
            }
        });

        try {
            SERVER_IP = InetAddress.getByAddress(ipAddr);
        } catch (UnknownHostException e) {
            Toast.makeText(this, "No Server IP", Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(this, "can't set location", Toast.LENGTH_LONG).show();
            return;
        }
        mMap.setMyLocationEnabled(true);
        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Criteria cr = new Criteria();
        Location location = lm.getLastKnownLocation(lm.getBestProvider(cr, false));
        if (location == null) {
            Toast.makeText(this, "can't set location1", Toast.LENGTH_LONG).show();
            return;
        }
        zoom(location);
        final double longitude = location.getLongitude();
        final double latitude = location.getLatitude();
        final String kind = getIntent().getStringExtra("kind");
        if (kind.equals("101") || kind.equals("104"))
            flag = true;
        Thread t = new Thread(new Runnable() {
            public void run() {
                if (kind.equals("101"))
                     toiletRecords = getToiletsOfRadiusR(kind, longitude, latitude);
                else if (kind.equals("104"))
                     toiletRecords = getToiletsByPreferences(kind, longitude, latitude);
                else if (kind.equals("102"))
                    ans = addToilet(kind, longitude, latitude);
            }
        });
        t.start();
        if (flag) {
            try {
                t.join();
                for (int i = 0; i <  toiletRecords.length; i++) {
                    mMap.addMarker(new MarkerOptions().position(new LatLng(Double.parseDouble( toiletRecords[i][3]), Double.parseDouble( toiletRecords[i][2]))).title( toiletRecords[i][1]).snippet(i + ""));
                    //the snippet in the marker (description) is the index of the markers attributes in toiletRecords array
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                // TODO Auto-geerated method stub
                chosenToilet = marker;
                marker.hideInfoWindow();
                ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getView().setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, screenHighPixels() / 2));
                tv_tn.setText(marker.getTitle());
                tv_oh.setText( toiletRecords[Integer.parseInt(chosenToilet.getSnippet())][6]);

                rb.setRating(Float.parseFloat( toiletRecords[Integer.parseInt(chosenToilet.getSnippet())][7]));
                iv.setImageResource(R.drawable.common_full_open_on_phone);
                getImage(toiletRecords[Integer.parseInt(chosenToilet.getSnippet())][0]);
                hideRateOptions();
                return true;
            }
        });
        if (kind.equals("102"))
            startActivity(new Intent(this, Home.class));
    }

    //101 request - get toilets by radius
    public String[][] getToiletsOfRadiusR(String kind, double lon, double lat) {
        String answer = null;
        try {
            System.out.println("connecting...");
            Socket socket = new Socket(SERVER_IP, SERVER_PORT);
            DataInputStream dataInputStream = new DataInputStream(socket.getInputStream());
            DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
            System.out.println("connected");
            dataOutputStream.writeUTF(kind + "#" + getIntent().getDoubleExtra("radius", 0) + "#" + lon + "#" + lat);
            dataOutputStream.flush();
            //answer = dataInputStream.readUTF();
            answer = dataInputStream.readLine();
            System.out.println("received " + answer);

            dataInputStream.close();
            dataOutputStream.close();
            socket.close();

        } catch (UnknownHostException ex) {
            System.out.println("UNKNOW HOST EXCEPTION");
        } catch (IOException ex) {
            System.out.println("IOEXCEPTION");
            System.out.println(ex.getMessage());
        }
        return makeResultFormat(answer);
    }

    //102 request - add a toilet to the data base
    private String addToilet(String kind, double longitude, double latitude) {
        Intent i = getIntent();
        String answer = null;
        try {
            System.out.println("connecting...");
            Socket socket = new Socket(SERVER_IP, SERVER_PORT);
            DataInputStream dataInputStream = new DataInputStream(socket.getInputStream());
            DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
            System.out.println("connected");

            dataOutputStream.writeUTF(kind + "#" + i.getStringExtra("name") + "#" + longitude + "#" + latitude + "#" + i.getDoubleExtra("price", 0)
                    + "#" + i.getDoubleExtra("cleanliness", 3) + "#" + i.getStringExtra("hours") + "#" + i.getDoubleExtra("rank", 3));
            dataOutputStream.flush();
            //answer = dataInputStream.readUTF();
            answer = dataInputStream.readLine();
            System.out.println("received " + answer);

            dataInputStream.close();
            dataOutputStream.close();
            socket.close();

        } catch (UnknownHostException ex) {
            System.out.println("UNKNOW HOST EXCEPTION");
        } catch (IOException ex) {
            System.out.println("IOEXCEPTION");
            System.out.println(ex.getMessage());
        }
        return answer;
    }

    //103 request - report a toilet
    public void notExist(View v) {
        final String toiletID =  toiletRecords[Integer.parseInt(chosenToilet.getSnippet())][0];
        ans = "1";
        if (!IDsInFile("reportedToiletsId.txt").contains(Integer.parseInt( toiletRecords[Integer.parseInt(chosenToilet.getSnippet())][0]))) {
            Thread t = new Thread(new Runnable() {
                public void run() {
                    try {
                        System.out.println("connecting...");
                        Socket socket = new Socket(SERVER_IP, SERVER_PORT);
                        DataInputStream dataInputStream = new DataInputStream(socket.getInputStream());
                        DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
                        System.out.println("connected");

                        dataOutputStream.writeUTF("103#" + toiletID);
                        dataOutputStream.flush();
                        ans = dataInputStream.readLine();
                        System.out.println("received " + ans);
                        dataInputStream.close();
                        dataOutputStream.close();
                        socket.close();
                    } catch (UnknownHostException ex) {
                        System.out.println("UNKNOW HOST EXCEPTION");
                    } catch (IOException ex) {
                        System.out.println("IOEXCEPTION");
                        System.out.println(ex.getMessage());
                    }
                }
            });
            t.start();
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(this, "You have already reported this toilet!", Toast.LENGTH_LONG).show();
        }

        ans = (ans != null && ans.length() > 4) ? ans.substring(4) : "";
        if (ans.equals("0")) {
            Toast.makeText(this, "Your report has been received", Toast.LENGTH_LONG).show();
            addIDToFile("reportedToiletsId.txt");
        }
        else if (ans.equals("1")) {
            Toast.makeText(this, "The toilet has been deleted", Toast.LENGTH_LONG).show();
            addIDToFile("reportedToiletsId.txt");
            chosenToilet.remove();
        }
        else {
            Toast.makeText(this, "Failed to report", Toast.LENGTH_LONG).show();
        }
    }

    //104 request - get toilets by preferences
    public String[][] getToiletsByPreferences(String kind, double lon, double lat) {
        String answer = null;
        try {
            System.out.println("connecting...");
            Socket socket = new Socket(SERVER_IP, SERVER_PORT);
            DataInputStream dataInputStream = new DataInputStream(socket.getInputStream());
            DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
            System.out.println("connected");

            dataOutputStream.writeUTF(kind + "#" + getIntent().getStringExtra("preferences") + "#" + lon + "#" + lat);
            dataOutputStream.flush();
            //answer = dataInputStream.readUTF();
            answer = dataInputStream.readLine();
            System.out.println("received " + answer);

            dataInputStream.close();
            dataOutputStream.close();
            socket.close();

        } catch (UnknownHostException ex) {
            System.out.println("UNKNOW HOST EXCEPTION");
        } catch (IOException ex) {
            System.out.println("IOEXCEPTION");
            System.out.println(ex.getMessage());
        }
        return makeResultFormat(answer);
    }

    //105 request - upload image to server
    public void goCamera(View v) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            iv.setImageBitmap(imageBitmap);/*}*/
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            System.out.println( toiletRecords[Integer.parseInt(chosenToilet.getSnippet())][0]);
            boolean ret = imageBitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
            byte[] byteArray = byteArrayOutputStream.toByteArray();
            final String encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);
            final String id =  toiletRecords[Integer.parseInt(chosenToilet.getSnippet())][0];

            Thread t = new Thread(new Runnable() {
                public void run() {
                    ans = sendImageToServer(encoded, id);
                }
            });
            t.start();
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if(ans.equals("0"))
            {
                Toast.makeText(this, "Image capture succeeded", Toast.LENGTH_SHORT).show();
            }
            else
            {
                Toast.makeText(this, "Image capture failed", Toast.LENGTH_SHORT).show();
            }
            getImage(id);
        }
    }

    public String sendImageToServer(String encoded, String id) {
        String answer = null;
        try {
            System.out.println("connecting...");
            Socket socket = new Socket(SERVER_IP, SERVER_PORT);
            DataInputStream dataInputStream = new DataInputStream(socket.getInputStream());
            BufferedReader br = new BufferedReader(new InputStreamReader(dataInputStream));
            DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
            ByteArrayOutputStream ao = new ByteArrayOutputStream();
            System.out.println("connected");
            String s = "105#" + id + "#";
            encoded = "#" + encoded;
            byte[] buf2 = encoded.getBytes(StandardCharsets.UTF_8);
            ao.write(buf2, 0, buf2.length);
            s += buf2.length;
            byte[] buf1 = s.getBytes(StandardCharsets.UTF_8);
            dataOutputStream.write(buf1);
            dataOutputStream.write(buf2);
            dataOutputStream.flush();
            answer = br.readLine();
            System.out.println("received " + answer);

            dataInputStream.close();
            dataOutputStream.close();
            socket.close();

        } catch (UnknownHostException ex) {
            System.out.println("UNKNOWN HOST EXCEPTION");
        } catch (IOException ex) {
            System.out.println("IOEXCEPTION");
            System.out.println(ex.getMessage());
        }
        return (answer != null && answer.length() > 4) ? answer.substring(4) : answer;
    }

    //106 request - get image from server
    public void getImage(String id) {
        final String fid = id;
        Thread t = new Thread(new Runnable() {
            public void run() {
                ans = getImageFromServer(fid);
            }
        });
        t.start();
        try {
            t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (!ans.equals("0")) {
            byte[] decodedString = Base64.decode(ans, Base64.DEFAULT);
            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            iv.setImageBitmap(decodedByte);
        }
    }

    public String getImageFromServer(String id) {
        String answer = null;
        try {
            System.out.println("connecting...");
            Socket socket = new Socket(SERVER_IP, SERVER_PORT);
            DataInputStream dataInputStream = new DataInputStream(socket.getInputStream());
            DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
            System.out.println("connected");

            dataOutputStream.writeUTF("106#" + id);
            dataOutputStream.flush();
            //answer = dataInputStream.readUTF();
            answer = dataInputStream.readLine();
            System.out.println("received " + answer);

            dataInputStream.close();
            dataOutputStream.close();
            socket.close();

        } catch (UnknownHostException ex) {
            System.out.println("UNKNOW HOST EXCEPTION");
        } catch (IOException ex) {
            System.out.println("IOEXCEPTION");
            System.out.println(ex.getMessage());
        }
        System.out.println(answer);
        return (answer != null && answer.length() > 4) ? answer.substring(4) : answer;
    }

    //107 request - rate the toilet
    public void rate(View v) {
        final String id =  toiletRecords[Integer.parseInt(chosenToilet.getSnippet())][0];
        final int clean = (int)rbc.getRating();
        final int rank = (int)rbr.getRating();
        Thread t = new Thread(new Runnable() {
            public void run() {
                ans = rate(id, clean, rank);
            }
        });
        t.start();
        try {
            t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if(ans.equals("0"))
        {
            addIDToFile("ratedToiletsId.txt"); //rating process completed
            Toast.makeText(this, "Rating succeeded", Toast.LENGTH_SHORT).show();
        }
        else
        {
            Toast.makeText(this, "Rating failed", Toast.LENGTH_SHORT).show();
        }
        hideRateOptions();
    }

    public String rate(String id, int clean, int rank) {
        String answer = null;
        try {
            System.out.println("connecting...");
            Socket socket = new Socket(SERVER_IP, SERVER_PORT);
            DataInputStream dataInputStream = new DataInputStream(socket.getInputStream());
            DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
            System.out.println("connected");

            dataOutputStream.writeUTF("107#" + id + "#" + clean + "#" + rank);
            dataOutputStream.flush();
            //answer = dataInputStream.readUTF();
            answer = dataInputStream.readLine();
            System.out.println("received " + answer);

            dataInputStream.close();
            dataOutputStream.close();
            socket.close();

        } catch (UnknownHostException ex) {
            System.out.println("UNKNOWN HOST EXCEPTION");
        } catch (IOException ex) {
            System.out.println("IOEXCEPTION");
            System.out.println(ex.getMessage());
        }
        System.out.println(answer);
        return (answer != null && answer.length() > 4) ? answer.substring(4) : answer;
    }

    //hides info window, shows rating fillings
    public void showRateOptions(View v)
    {
        if(IDsInFile("ratedToiletsId.txt").contains(Integer.parseInt( toiletRecords[Integer.parseInt(chosenToilet.getSnippet())][0])))
        {
            Toast.makeText(this, "You have already rated this toilet!", Toast.LENGTH_LONG).show();
            return;
        }

        gl1.setVisibility(View.GONE);
        gl2.setVisibility(View.VISIBLE);
    }

    //hides info rating fillings, shows info window
    public void hideRateOptions()
    {
        gl2.setVisibility(View.GONE);
        gl1.setVisibility(View.VISIBLE);
    }

    //routes to chosenToilet
    public void route(boolean isDrive) {
        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Criteria cr = new Criteria();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        Location location = lm.getLastKnownLocation(lm.getBestProvider(cr, false));

        // Getting URL to the Google Directions API
        String url = getDirectionsUrl(new LatLng(location.getLatitude(), location.getLongitude()),
                new LatLng(Double.parseDouble( toiletRecords[Integer.parseInt(chosenToilet.getSnippet())][3]), Double.parseDouble( toiletRecords[Integer.parseInt(chosenToilet.getSnippet())][2])),
                isDrive);

        downloadTask = new DownloadTask(mMap,routesDoneInActivity);

        // Start downloading json data from Google Directions API
        downloadTask.execute(url);

        routesDoneInActivity++;
    }

    //asks the client if routing should be through walking or driving
    public void showAlertDialog(View v)
    {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(this, android.R.style.Theme_DeviceDefault_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(this);
        }
        builder.setTitle("route options")
                .setMessage("how do you wish to arrive?")
                .setPositiveButton("walking", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        route(false);
                    }
                })
                .setNegativeButton("driving", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        route(true);
                    }
                })
                .show();
    }

    //url builder for url to download route directions
    private String getDirectionsUrl(LatLng origin, LatLng dest, boolean isDrive) {

        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

        // Sensor enabled
        String sensor = "sensor=false";

        // Mode is driving or walking
        String mode= "mode=" + (isDrive? "driving" : "walking");

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor + "&" + mode;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;

        return url;
    }

    //returns screen hight in pixels
    private int screenHighPixels() {
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        return dm.heightPixels;
    }

    //zoom on current location
    private void zoom(Location location)
    {
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 13));
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(new LatLng(location.getLatitude(), location.getLongitude()))      // Sets the center of the map to location user
                .zoom(17)                   // Sets the zoom
                .tilt(40)                   // Sets the tilt of the camera to 30 degrees
                .build();                   // Creates a CameraPosition from the builder
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    //organizes server answer into array (toiletRecords)
    private String[][] makeResultFormat(String answer) {
        ArrayList<String> doubleAsText = new ArrayList<>();
        int k = 0;
        if (answer != null && answer.length() > 0)
            for (String retval : answer.split(",")) {
                if (k != 0) {
                    doubleAsText.add(retval);
                }
                k++;
            }

        String[][] results = new String[doubleAsText.size() / 12][12]; //12 is the number of parameters of each toilet
        k = 0;
        String str = "";

        for (int i = 0; i < results.length; i++) {
            for (int j = 0; j < results[i].length; j++) {
                results[i][j] = doubleAsText.get(k++);
                str += results[i][j] + " ";
            }
            str += "\n";
        }

        System.out.print(str);
        return results;
    }

    //returns IDs written in the file which name received as parameter
    public ArrayList<Integer> IDsInFile(String fileName) {
        ArrayList<Integer> IDs = new ArrayList<>();
        File root = new File(Environment.getExternalStorageDirectory(), "ToiletFinderNotes");
        if (!root.exists())
            root.mkdirs();
        File file = new File(root, fileName);
        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;
            for (int i = 0; (line = br.readLine()) != null; i++) {

                IDs.add(Integer.parseInt(line));
            }
            br.close();
        } catch (IOException e) {
            Toast.makeText(this, "problem reading " + fileName, Toast.LENGTH_LONG).show();
        }
        return IDs;
    }

    //writes ID to the file which name received as parameter
    public void addIDToFile(String FileName)
    {
        File root = new File(Environment.getExternalStorageDirectory(), "ToiletFinderNotes");
        if (root.exists()) {
            try {
                if (!root.exists())
                    root.mkdirs();
                File file = new File(root, FileName);
                PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(file, true)));
                out.println( toiletRecords[Integer.parseInt(chosenToilet.getSnippet())][0]);
                out.close();
                Toast.makeText(this, "Saved", Toast.LENGTH_SHORT).show();
            } catch (IOException e) {
                e.printStackTrace();
                Toast.makeText(this, "Failed to save", Toast.LENGTH_LONG).show();
            }
        }
    }
}

