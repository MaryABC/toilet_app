package com.example.user.toiletapp1;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.RatingBar;
import android.widget.Toast;

public class AddToilet extends AppCompatActivity {

    EditText et1,et2;
    RatingBar rtb1,rtb2;
    NumberPicker np1,np2,np3,np4;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_toilet);
        et1 = (EditText) findViewById(R.id.editText2);
        et2 = (EditText) findViewById(R.id.editText3);
        rtb1 = (RatingBar) findViewById(R.id.ratingBar1);
        rtb2 = (RatingBar) findViewById(R.id.ratingBar2);
        np1 = (NumberPicker) findViewById(R.id.numberPicker1);
        np2 = (NumberPicker) findViewById(R.id.numberPicker2);
        np3 = (NumberPicker) findViewById(R.id.numberPicker3);
        np4 = (NumberPicker) findViewById(R.id.numberPicker4);
        np1.setMinValue(0); np3.setMinValue(0);
        np1.setMaxValue(23); np3.setMaxValue(23);
        np2.setMinValue(0); np4.setMinValue(0);
        np2.setMaxValue(59); np4.setMaxValue(59);
        rtb1.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                if(rating<1.0f)
                    ratingBar.setRating(1.0f);
            }
        });
        rtb2.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                if(rating<1.0f)
                    ratingBar.setRating(1.0f);
            }
        });
    }

    public void passToMap2(View v)
    {
        Intent t= new Intent(this, MapsActivity.class);
        t.putExtra("kind","102");
        t.putExtra("name",et1.getText().toString());
        try{t.putExtra("price",Double.parseDouble(et2.getText().toString()));}
        catch (NumberFormatException ex){t.putExtra("price",0);}
        t.putExtra("cleanliness",(double)rtb1.getNumStars());
        t.putExtra("hours",zeroBefore(np1.getValue())+":"+zeroBefore(np2.getValue())+
                       "-"+zeroBefore(np3.getValue())+":"+zeroBefore(np4.getValue()));
        t.putExtra("rank",(double)rtb2.getNumStars());
        startActivity(t);
    }

    public static String zeroBefore(int a)
    {
        if(a<10)
            return "0"+a;
        return ""+a;
    }
}
