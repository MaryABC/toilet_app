package com.example.user.toiletapp1;

import android.content.Context;
import android.graphics.Path;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.List;

public class DefaultSettings extends AppCompatActivity {

    EditText et1,et2,et3,et4;
    Switch s1,s2;
    Button btn;
    NumberPicker np1,np2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_default_settings);
        et1 = (EditText) findViewById(R.id.editText4);
        //et2 = (EditText) findViewById(R.id.editText5);
        et3 = (EditText) findViewById(R.id.editText6);
       // et4 = (EditText) findViewById(R.id.editText7);
        np1 = (NumberPicker) findViewById(R.id.numberPicker5);
        np2 = (NumberPicker) findViewById(R.id.numberPicker6);
        s1 = (Switch) findViewById(R.id.switch1);
        s2 = (Switch) findViewById(R.id.switch2);
        btn = (Button) findViewById(R.id.button5);
        np1.setMinValue(1); np2.setMinValue(1);
        np1.setMaxValue(5); np2.setMaxValue(5);
        showDefaultSettings();
    }

    public void showDefaultSettings() {
        File root = new File(Environment.getExternalStorageDirectory(), "ToiletFinderNotes");
        if (!root.exists()) {
            return;
        }
        File file = new File(root, "defaultSettings.txt");
        /*File sdcard = Environment.getExternalStorageDirectory();
        File file = new File(sdcard,"defaultSettings.txt");*/
        String[] text = new String[6];
        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;
            for (int i=0; (line = br.readLine())!= null && i<6;i++) {
                text[i]=line;
            }
            br.close();
        }
        catch (IOException e) {
            Toast.makeText(this,"problem reading settings",Toast.LENGTH_LONG).show();
        }
        setSettingsToFileData(text);
    }

    private void setSettingsToFileData(String[] text) {
        if(text[0].equals("1"))
            s1.setChecked(true);
        else
            s1.setChecked(false);
        et1.setText(text[1]);
        if(text[2].equals("1"))
            s2.setChecked(true);
        else
            s2.setChecked(false);
        //et2.setText(text[3]);
        np1.setValue((int)Double.parseDouble(text[3]));
        et3.setText(text[4]);
        np2.setValue((int)Double.parseDouble(text[5]));
        //et4.setText(text[5]);
    }
/*
*  <EditText
            android:id="@+id/editText5"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:text="Cleanliness"
            android:textSize="19dp"
            />*/
    public void saveSettings(View v)
    {
        try {
            File root = new File(Environment.getExternalStorageDirectory(), "ToiletFinderNotes");
            if (!root.exists()) {
                if(root.mkdirs()) Toast.makeText(this, "1", Toast.LENGTH_SHORT).show();;
            }
            File file = new File(root, "defaultSettings.txt");
            FileWriter writer = new FileWriter(file);
            if(getSettings()!=null)
                writer.write(getSettings());
            writer.flush();
            writer.close();
            Toast.makeText(this, "Saved", Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String getSettings() {
        String res=null;
        try{
            res = "";
            res += (s1.isChecked()? "1":"0")+"\n";
            res += Double.parseDouble(et1.getText().toString())+"\n";
            res += (s2.isChecked()? "1":"0")+"\n";
            //res += Double.parseDouble(et2.getText().toString())+"\n";
            res+=np1.getValue()+"\n";
            res += Double.parseDouble(et3.getText().toString())+"\n";
            //res += Double.parseDouble(et4.getText().toString());
            res+=np2.getValue()+"\n";
        }
        catch (NumberFormatException ex){
            res += 1+"\n";
            Toast.makeText(this, "At least one of the parameters is incorrect\n", Toast.LENGTH_LONG).show();
        }
        return res;
    }

}
