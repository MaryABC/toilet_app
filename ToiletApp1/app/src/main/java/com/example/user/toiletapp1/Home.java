package com.example.user.toiletapp1;

import android.*;
import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.style.AlignmentSpan;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import static android.R.attr.bitmap;
import static java.security.AccessController.getContext;

public class Home extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,OnMapReadyCallback {

    private GoogleMap mMap;
    //TextView tv;
    String[] settings=new String[6];
    //Button btn1,btn2;
    String ans;
    //ImageView iv;
    private static final int SERVER_PORT = 8888;
    byte[] ipAddr = new byte[]{(byte) 35, (byte) 162, (byte) 187, (byte) 156};
    private InetAddress SERVER_IP = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //tv = (TextView) findViewById(R.id.textView13);
        //btn1 = (Button) findViewById(R.id.button6);
        //btn2 = (Button) findViewById(R.id.button7);
        //iv = (ImageView) findViewById(R.id.imageView2);
        foo();

        final Intent t=new Intent(this, AddToilet.class);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_action_add_location));
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(t);
                /*Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();*/
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        try {
            SERVER_IP = InetAddress.getByAddress(ipAddr);
        } catch (UnknownHostException e) {
            Toast.makeText(this, "No Server IP", Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map1);
        mapFragment.getMapAsync(this);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(this, "can't set location", Toast.LENGTH_LONG).show();
            return;
        }
        mMap.setMyLocationEnabled(true);
        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Criteria cr = new Criteria();
        Location location = lm.getLastKnownLocation(lm.getBestProvider(cr, false));
        if (location == null) {
            Toast.makeText(this, "can't set location1", Toast.LENGTH_LONG).show();
            return;
        }
        if (location != null) {
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 13));

            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(new LatLng(location.getLatitude(), location.getLongitude()))      // Sets the center of the map to location user
                    .zoom(17)                   // Sets the zoom
                    .tilt(40)                   // Sets the tilt of the camera to 30 degrees
                    .build();                   // Creates a CameraPosition from the builder
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        foo();
        Intent t;
        if (id == R.id.Finding_by_radius) {
            t = settings[0].equals("1") ? new Intent(this, MapsActivity.class) : new Intent(this, MainActivity.class);
            if(settings[0].equals("1")) {
                t.putExtra("kind","101");
                t.putExtra("radius",Double.parseDouble(settings[1]));
            }
            startActivity(t);
        } else if (id == R.id.Finding_by_preferences) {
            t = settings[0].equals("1") ? new Intent(this, MapsActivity.class) : new Intent(this, EnterPreferences.class);
            if(settings[0].equals("1")) {
                t.putExtra("kind","104");
                t.putExtra("preferences",settings[2]+"#"+settings[4]+"#"+settings[3]);
            }
            startActivity(t);
        } else if (id == R.id.add_toilet) {
            t = new Intent(this, AddToilet.class);
            startActivity(t);
        } else if (id == R.id.report_toilet) {
            t = new Intent(this,DefaultSettings.class);
            startActivity(t);
        } else if (id == R.id.nav_share) {
            t =new Intent(Intent.ACTION_SEND);
            t.setType("text/plain");
            t.putExtra(Intent.EXTRA_TEXT, "Use Toilet Finder! an excellent application");
            startActivity(Intent.createChooser(t,"Share your good experience"));
        } else if (id == R.id.nav_send) {
            t = new Intent(Intent.ACTION_SEND);
            t.setType("message/rfc822");
            t.putExtra(Intent.EXTRA_EMAIL  , new String[]{"danshobshub@gmail.com","maryarav@gmail.com"});
            t.putExtra(Intent.EXTRA_SUBJECT, "subject of email");
            t.putExtra(Intent.EXTRA_TEXT   , "body of email");
            try {
                startActivity(Intent.createChooser(t, "Send mail..."));
            } catch (android.content.ActivityNotFoundException ex) {
                Toast.makeText(this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
            }
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void foo() {
        File root = new File(Environment.getExternalStorageDirectory(), "ToiletFinderNotes");
        if (!root.exists()) {
            return;
        }
        File file = new File(root, "defaultSettings.txt");
        /*File sdcard = Environment.getExternalStorageDirectory();
        File file = new File(sdcard,"defaultSettings.txt");*/
        String text ="";
        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;
            for (int i=0; (line = br.readLine())!= null && i<6;i++) {
                text += line+"\n";
                settings[i]=line;
            }
            br.close();
        }
        catch (IOException e) {
            Toast.makeText(this,"problem reading settings",Toast.LENGTH_LONG).show();
        }
        //tv.setText(text);
    }
}


/*
 <ImageView
        android:id="@+id/imageView2"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        app:srcCompat="@mipmap/ic_launcher" />

    <LinearLayout
        android:layout_width="352dp"
        android:layout_height="wrap_content"
        android:orientation="horizontal">

        <Button
            android:id="@+id/button6"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:gravity="center"
            android:onClick="goCamera"
            android:text="upload photo"
            tools:layout_editor_absoluteX="186dp"
            tools:layout_editor_absoluteY="350dp" />
        <TextView
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:text="                   "
            android:gravity="center"
            android:textSize="22dp" />
        <Button
            android:id="@+id/button7"
            android:layout_width="125dp"
            android:layout_height="wrap_content"
            android:gravity="center"
            android:onClick="getImage"
            android:text="show photo"
            tools:layout_editor_absoluteX="186dp"
            tools:layout_editor_absoluteY="350dp" />

    </LinearLayout>

    <TextView
        android:id="@+id/textView13"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="TextView"
        android:gravity="center"
        android:textSize="22dp"
        tools:layout_editor_absoluteX="176dp"
        tools:layout_editor_absoluteY="284dp" />
 */






