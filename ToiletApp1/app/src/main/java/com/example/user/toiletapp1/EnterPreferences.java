package com.example.user.toiletapp1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class EnterPreferences extends AppCompatActivity {
    RadioButton[][] rb= new RadioButton[3][];
    int[][] values={{1,0},{0,1,2,5,10},{1,2,3,4,5}};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_preferences);
        rb[0] = new RadioButton[2];
        rb[1] = new RadioButton[5];
        rb[2] = new RadioButton[5];
        rb[0][0]= (RadioButton) findViewById(R.id.radioButton);
        rb[0][1]= (RadioButton) findViewById(R.id.radioButton2);
        rb[1][0]= (RadioButton) findViewById(R.id.radioButton4);
        rb[1][1]= (RadioButton) findViewById(R.id.radioButton5);
        rb[1][2]= (RadioButton) findViewById(R.id.radioButton6);
        rb[1][3]= (RadioButton) findViewById(R.id.radioButton7);
        rb[1][4]= (RadioButton) findViewById(R.id.radioButton8);
        rb[2][0]= (RadioButton) findViewById(R.id.radioButton9);
        rb[2][1]= (RadioButton) findViewById(R.id.radioButton10);
        rb[2][2]= (RadioButton) findViewById(R.id.radioButton11);
        rb[2][3]= (RadioButton) findViewById(R.id.radioButton12);
        rb[2][4]= (RadioButton) findViewById(R.id.radioButton13);
    }

    public void passToMap(View v)
    {
        Intent t= new Intent(this, MapsActivity.class);
        t.putExtra("kind","104");
        t.putExtra("preferences",getPreferences());
        System.out.println(getPreferences());
        startActivity(t);
    }

    public String getPreferences()
    {
        String result="";
        for(int i=0;i<rb.length;i++)
        {
            for(int j=0;j<rb[i].length;j++)
            {
                if(rb[i][j].isChecked())
                    result+=values[i][j]+"#";
            }
        }
        return result.substring(0,result.length()-1);
    }
}
