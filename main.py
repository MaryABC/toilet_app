import threading
import socket
import time
import os
import sqlite3
import base64
from math import radians,cos,sin,asin,sqrt

__author__ = 'Dan&Mary'

IP = "0.0.0.0"
PORT = 8888


class ThreadedServer(object):
    def __init__(self, host, port):
        self.host = host
        self.port = port
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.sock.bind((self.host, self.port))

    def listen(self):
        self.sock.listen(5)
        while True:
            client, address = self.sock.accept()
            client.settimeout(10000)
            threading.Thread(target = self.listenToClient,args = (client,address)).start()

    def listenToClient(self, client, address):
        size = 5
        print "\nclient connected"
        while True:
            try:
                data = client.recv(size)
                print "data- \"",data,"\""
                print "received client"
                if data:
                    if "105" in data:
                        data = data + client.recv(9)
                        size = data.split('#')[2]
                        data = data + recvall(client,int(size))
                    else:
                        size = 1024
                        data = data + client.recv(size)
                    response = prepare_data(data.split('#'))
                    if response[0:3]=='206':
                        print "response - image send"
                    else:
                        print "response - " + response
                    client.send(response)
                    client.close()
                else:
                    raise Exception('data is empty!')
            except:
                client.close()
                return False
        return True


def recvall(sock, n):
    # Helper function to recv n bytes or return None if EOF is hit
    data = ''
    s=0
    while len(data) < n:
        packet = sock.recv(1024)
        if not packet:
            return None
        s+=len(packet)
        data += packet
    return data


def haversine(lon1, lat1,lon2, lat2):
    lon1, lat1, lon2, lat2=map(radians,[float(lon1), float(lat1),float(lon2), float(lat2)])
    dlon=lon2-lon1
    dlat=lat2-lat1
    a=sin(dlat/2)**2+cos(lat1)*cos(lat2)*sin(dlon/2)**2
    c=2*asin(sqrt(a))
    km=6371*c
    return km


def get_lonlat_of_toilets_in_database():
    db = sqlite3.connect(r'database.db', isolation_level=None)
    c = db.cursor()
    c.execute('''SELECT * FROM toilets_table''')
    a = c.fetchall()
    print "found in db - ",a
    db.close()
    return a


def make_string(s):
    res = ''
    s=list(s)
    for i in xrange(0,len(s)):
        s[i]=list(s[i])
        s[i][1]=s[i][1].encode('ascii')
        s[i][6]=s[i][6].encode('ascii')
        if s[i][10] is not None:
            s[i][10]=s[i][10].encode('ascii')
    s=str(s)
    for i in xrange(0,len(s)):
        for j in xrange(0,len(s[i])):
            if s[i][j] != ' ' and s[i][j] != '[' and s[i][j] != ']' and s[i][j] != '\'' and s[i][j] != '\"':
                res += str(s[i][j])
    return res


def find_r_close(data):
    r=float(data[1])
    lonlat=get_lonlat_of_toilets_in_database()
    return [lonlat[x] for x in xrange(0,len(lonlat)) if haversine(lonlat[x][2],lonlat[x][3],data[2],data[3])<r]


def insert_to_toilet_db(data):
    db = sqlite3.connect('database.db', isolation_level=None)
    c = db.cursor()
    print "a"
    c.execute('''INSERT INTO toilets_table(name, lon, lat, price, clean, hours, rank, delcounter, ratecount) VALUES(?,?,?,?,?,?,?,?)''',
              (data[1], data[2],data[3], data[4],data[5], data[6], data[7], 0, 1))
    db.close()
    print "a"
    return "0\n"


def delete_from_toilet_db(data):
    db = sqlite3.connect('database.db', isolation_level=None)
    c = db.cursor()
    c.execute('''SELECT * FROM toilets_table WHERE id = '''+data[1])
    a=c.fetchall()
    print a,"\t",a[0][-3],a[0][-4]
    if int(a[0][-4])>2:
        c.execute('''DELETE FROM toilets_table WHERE id = '''+data[1])
        db.close()
        return "1\n"
    else:
        c.execute('''UPDATE toilets_table SET delcounter = '''+str(int(a[0][-4])+1)+''' WHERE id = '''+data[1])
        db.close()
        return "0\n"
    print "ERROR!!!"


def find_with_preferences(data):
    db = sqlite3.connect('database.db', isolation_level=None)
    c = db.cursor()
    c.execute('''SELECT * FROM toilets_table WHERE price <= '''+data[2]+''' AND clean >= '''+data[3])
    a=c.fetchall()
    print "a - ",a
    if (data[1]==1):
        for i in xrange(0,len(a)):
            a[i]=list(a[i])
            a[i].append(haversine(a[i][2], a[i][3], data[4], data[5]))
            print haversine(a[i][2], a[i][3], data[4], data[5])
        a.sort(key=lambda a: a[-1])
    db.close()
    #lonlat = [(a[x][2],a[x][3]) for x in xrange(0,len(a))]
    return a[0:min(len(a),20)]


def upload_image(data):
    toilet_id, size, img = data[1],data[2],data[3]
    db = sqlite3.connect('database.db', isolation_level=None)
    c = db.cursor()
    s=create_photo_in_images(toilet_id,img)
    print "s - ",s
    c.execute('''UPDATE toilets_table SET img_size = ''' + size + ''' WHERE id = ''' + toilet_id)
    c.execute('''UPDATE toilets_table SET img = '''+s+''' WHERE id = '''+toilet_id)
    db.close()
    return "0\n"


def create_photo_in_images(toilet_id,img):
    print "saving photo in images"
    path="C:\\Users\\Administrator\\Desktop\\project\\images\\"+toilet_id+".png"
    open(path,'w').close()
    f = open(path, "a")
    f.write(img)
    f.close()
    return "'"+"C:\\Users\\Administrator\\Desktop\\project\\images\\"+toilet_id+".png"+"'"


def get_image(data):
    toilet_id=data[1]
    db = sqlite3.connect('database.db', isolation_level=None)
    c = db.cursor()
    c.execute('''SELECT img FROM toilets_table WHERE id = ''' + toilet_id)
    a=c.fetchall()
    print "a - ",a
    a=str(a[0])[3:-3]
    if len(a)<5:
        return "0\n"
    print "a - ",a
    f=open(a)
    a=f.read()
    a=a.replace('\n','')
    f.close()
    db.close()
    return a
        
def rate_toilet(data):
    toilet_id=data[1]
    clean=int(data[2])
    rank=int(data[3])
    db = sqlite3.connect('database.db', isolation_level=None)
    c = db.cursor()
    c.execute('''SELECT clean,rank,ratecount FROM toilets_table WHERE id = ''' + toilet_id)
    a=c.fetchall()
    a=a[0]
    new_values=[((float(a[0])*int(a[2])+clean)/(int(a[2])+1)),((float(a[1])*int(a[2])+rank)/(int(a[2])+1)),int(a[2])+1]
    print new_values
    print '''UPDATE toilets_table SET clean = ''',new_values[0],''', rank = ''',new_values[1],''', ratecount = ''',new_values[2],''' WHERE id = ''',toilet_id
    c.execute('''UPDATE toilets_table SET clean = ''' + str(new_values[0])+ ''', rank = '''+str(new_values[1])+
              ''', ratecount = '''+str(new_values[2]) + ''' WHERE id = ''' + toilet_id)
    db.close()
    return "0\n"

    
def prepare_data(data):
    data[0]=data[0][-3:]
    print "data - ", data
    if data[0] == '101':
        res = "201," + make_string(find_r_close(data))
    elif data[0] == '102':
        res = "202," + insert_to_toilet_db(data)
    elif data[0] == '103':
        print "Asdas"
        res = "203," + delete_from_toilet_db(data)
        print "adfnog"
    elif data[0] == '104':
        res = "204," + make_string(find_with_preferences(data))
    elif data[0] == '105':
        res = "205," + upload_image(data)
    elif data[0] == '106':
        res = "206," + get_image(data)
    elif data[0] == '107':
        res = "207," + rate_toilet(data)
    else:
        res="wrong request number\n"
    if data[0] != '106':
        print "result - ", res
    return res


def main():
    ThreadedServer('', PORT).listen()


if __name__ == "__main__":
    main()



